package misc;

import fxmlControllers.ControllerImageTest;
import javafx.scene.canvas.GraphicsContext;
import javafx.stage.Stage;

public class Exchange {
	
	public static class ExchangeInner
	{
		public static Stage primaryStage;
		public static GraphicsContext graphicsContext;
		public static ControllerImageTest cIT;
		
		
		public static void setStage(Stage pStage){
			primaryStage = pStage;
		}
		
		public static Stage getStage(){
			return primaryStage;
		}
		
		public static void setGraphicsContext(GraphicsContext pGraphicsContext){
			graphicsContext = pGraphicsContext;
		}
		
		public static GraphicsContext getGraphicsContext(){
			return graphicsContext;
		}
		
		public static void setCIT(ControllerImageTest pCIT){
			cIT = pCIT;
		}
		
		public static ControllerImageTest getCIT(){
			return cIT;
		}
	}

}
