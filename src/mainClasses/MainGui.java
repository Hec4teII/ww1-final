package mainClasses;


import fxmlControllers.ControllerImageTest;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mainClasses.PlayerSettings.PlayerSetSettings;
import misc.Exchange.ExchangeInner;


public class MainGui extends Application {
	
		ControllerImageTest fs = null;
	
		public static void main(String[] args) 
		{
			launch(args);
		}
	
		@Override
		public void start(Stage primaryStage) throws Exception
		{
	    	ExchangeInner.setStage(primaryStage);
	    	primaryStage.setFullScreen(true);
	    	
	    	FXMLLoader loader = new FXMLLoader(getClass().getResource("/fxmlFiles/ImageTest.fxml"));
	    	Parent root = (Parent) loader.load();
	        
	    	primaryStage.setTitle("Polytope");
	        PlayerSetSettings.setResolution(3);   
	        Scene newScene = new Scene(root,PlayerSetSettings.getResolutionWidth(),PlayerSetSettings.getResolutionHeight());
	        
	        fs = (ControllerImageTest) loader.getController();
	        ExchangeInner.setCIT(fs);
	        
	        ThreadClock tc = new ThreadClock();
	 	   	tc.startClock();
	        
	        primaryStage.setScene(newScene);
	        primaryStage.show();
	        
	        primaryStage.setOnCloseRequest(event -> {
	 	        System.exit(1);
	 	    });
		}

    
    
    
}