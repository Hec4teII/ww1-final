package mainClasses;

import static java.util.concurrent.TimeUnit.*;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;

import misc.Exchange.ExchangeInner;

public class ThreadClock
{
   private final ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
   private Runnable Thread = new ThreadContent();
   private boolean paused = false;

   public void startClock() 
   {
     ScheduledFuture<?> ThreadHandle = scheduler.scheduleAtFixedRate(Thread, 0, 1,SECONDS);
   }
   
   public void pause() throws InterruptedException
   {
	   if(paused){
		   Thread.notify();
	   }else {
		   Thread.wait(); 
	   }
   }

   	private class ThreadContent implements Runnable
   	{
		@Override
		public void run(){
			ExchangeInner.getCIT().onTick();	
		}
   }
 }