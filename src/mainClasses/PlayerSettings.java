package mainClasses;

public class PlayerSettings {
	
	public static class PlayerSetSettings
	{
		private static int resolutionWidth = 720;
		private static int resolutionHeight = 480;
		private static String playerName;
		
		public static void setResolution(int pInt)
		{
			switch(pInt)
			{
			   case 1 :
				  resolutionWidth = 720;
				  resolutionHeight = 480;
			      break;
			   
			   case 2 :
				  resolutionWidth = 1280;
				  resolutionHeight = 960;
			      break;
			      
			   case 3 :
				   resolutionWidth = 1920;
				   resolutionHeight = 1080;
				   break;
			      
			   default : 
			}		
		}

		public static int getResolutionWidth() {
			return resolutionWidth;
		}

		public static int getResolutionHeight() {
			return resolutionHeight;
		}

		public static String getPlayerName() {
			return playerName;
		}

		public static void setPlayerName(String playerName) {
			PlayerSetSettings.playerName = playerName;
		}
		
	}

}
