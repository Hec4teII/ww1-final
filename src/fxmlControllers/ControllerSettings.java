package fxmlControllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mainClasses.PlayerSettings.PlayerSetSettings;

public class ControllerSettings {
	
	
	public ControllerSettings()
	{

	}
	
	
	public void onBackButton(ActionEvent event) throws IOException
    {
    	Parent newSceneContent = FXMLLoader.load(getClass().getResource("/fxmlFiles/MainMenu.fxml"));
    	Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
    	Scene newScene = new Scene(newSceneContent,PlayerSetSettings.getResolutionWidth(),PlayerSetSettings.getResolutionHeight());	
    	window.setScene(newScene);
    	window.show();
    }
	
	
}
