package fxmlControllers;

import java.io.IOException;

import javafx.event.ActionEvent;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import mainClasses.PlayerSettings.PlayerSetSettings;

public class ControllerMainMenu {
	
	
	public void onPlayButton(ActionEvent event) throws IOException
    {
    	Parent newSceneContent = FXMLLoader.load(getClass().getResource("/fxmlFiles/ImageTest.fxml"));
    	Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
    	Scene newScene = new Scene(newSceneContent,PlayerSetSettings.getResolutionWidth(),PlayerSetSettings.getResolutionHeight());
    	window.setScene(newScene);
    	window.show();
    }
	
	public void onSettingsButton(ActionEvent event) throws IOException
    {
		Parent newSceneContent = FXMLLoader.load(getClass().getResource("/fxmlFiles/Settings.fxml"));
    	Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
    	Scene newScene = new Scene(newSceneContent,PlayerSetSettings.getResolutionWidth(),PlayerSetSettings.getResolutionHeight());
    	window.setScene(newScene);
    	window.show();
    }
	
	public void onQuitButton(ActionEvent event) throws IOException
    {
		Stage window = (Stage) ((Node)event.getSource()).getScene().getWindow();
		window.close();
    }

}
