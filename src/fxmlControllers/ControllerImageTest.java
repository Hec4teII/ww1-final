package fxmlControllers;

import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;

public class ControllerImageTest
{
	@FXML ImageView Afghanistan,�gypten,Alaska,Algerien,Angola,Antarktis,Argentinien,Australien,Balkanstaaten,BaltischeStaaten,Bolivien,Botswana,Brasilien,
					China,Deutschland,Finnland,Frankreich,GB,Gr�nland,Italien,Indien,Indonesien,Irak,Iran,Island,Japan,Kanada,Karibik,Kasachstan,Kolumbien,Libyen,
					Madagaskar,Marokko,Mexico,Mittelamerika,NaherOsten,Neuguinea,Neuseeland,Nordkorea,Norwegen,Ostafrika,Pakistan,Peru,Phillipinen,Polen,Russland,
					SaudiArabien,Schweden,Simbabew,Spanien,S�dafrika,Sudan,S�dkorea,Simbabwe,S�dostasien,T�rkei,Ukraine,USA,Westafrika,Zentralafrika,Zentralasien,Zentraleuropa;
	
	@FXML ImageView m1p,m1a,m2p,m2a,m3p,m3a,featuresIV;
	@FXML Label percent,cash;
	@FXML Pane pauseGrey;
	@FXML ProgressBar p1,p2,p3,p4,pb1;
	@FXML ImageView b1p,b1a,b2p,b2a,b3p,b3a;
	
	private int counter = 1;
	
	public void onClicked(){
		
	}
	
	public void menuButton()
	{
		/*
		if(mainMenu.isVisible()){
			mainMenu.setVisible(false);
			pauseGrey.setVisible(false);
			//tc.pause();
		}
		else {
			mainMenu.setVisible(true);
			pauseGrey.setVisible(true);
		}
		*/
	}
	
	public void onTick()
	{
		Platform.runLater(new Runnable() {
            @Override public void run() {
            	China.setOpacity(Math.random());
            	Russland.setOpacity(Math.random());
            	Deutschland.setOpacity(Math.random());
            	Australien.setOpacity(Math.random());
            	Indien.setOpacity(Math.random());
            	pb1.setProgress(pb1.getProgress()+0.01);
            	
            	if(counter!=100){
            		counter++;
            	}
            	
            	percent.setText(Integer.toString(counter) + "%");
            	if(counter<=25){
            		p1.setProgress(p1.getProgress()+0.04);
            	}else if(counter<=50){
            		p2.setProgress(p2.getProgress()+0.04);
            	}else if(counter<=75){
            		p3.setProgress(p3.getProgress()+0.04);
            	}else if(counter<100){
            		p4.setProgress(p4.getProgress()+0.04);
            	}
            	
            	if(counter == 25){
            		m1a.setOpacity(1);
            	}else if(counter == 50){
            		m2a.setOpacity(1);
            	}else if(counter == 75){
            		m3a.setOpacity(1);
            	}
            }
        });
	}
	
	public void onFeatures(){
		b1a.setOpacity(1);
		b2a.setOpacity(0);
		b3a.setOpacity(0);
	}
	
	public void onSecurity(){
		b1a.setOpacity(0);
		b2a.setOpacity(1);
		b3a.setOpacity(0);
	}
	
	public void onUtility(){
		b1a.setOpacity(0);
		b2a.setOpacity(0);
		b3a.setOpacity(1);
	}
	
	 @FXML
	    public void initialize(){
		 b1a.setOpacity(1);
	    }
	    

	
}
